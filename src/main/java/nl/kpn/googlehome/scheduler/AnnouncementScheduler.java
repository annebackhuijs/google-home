package nl.kpn.googlehome.scheduler;

import nl.kpn.googlehome.model.VoiceStore;
import nl.kpn.googlehome.service.VoiceStoreService;
import nl.kpn.googlehome.vo.Announcement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Component
public class AnnouncementScheduler {
    @Value("${googlehome.url}")
    private String url;
    @Value("${googlehome.messagePrefix}")
    private String messagePrefix;

    private RestTemplate restTemplate;

    @Autowired VoiceStoreService voiceStoreService;

    public AnnouncementScheduler(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    //fixedRate = 30000  :   Every 30 seconds
    //fixedRate = 86400000  : Every Day
    //@Scheduled(fixedRate = 60000)
    @Scheduled(cron="0 * * * * ?")
    @Async
    public void importDefaultUnifiData() {
        List<VoiceStore> voicestoreList= voiceStoreService.getVoicestoreList();

        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdfTime  = new SimpleDateFormat("HH:mm");

        String dateNow = simpleDateFormat.format(new Date());
        String timeNow = sdfTime.format(new Date());

        for (VoiceStore voiceStore : voicestoreList) {
            String announcementDate = simpleDateFormat.format(voiceStore.getAnnouncementDate());
            String announcementTime = sdfTime.format(voiceStore.getAnnouncementDate());
            boolean shouldBeAnnounced = voiceStore.getAnnounced()==null || !voiceStore.getAnnounced().equalsIgnoreCase("Y");
            if (dateNow.equalsIgnoreCase(announcementDate) && timeNow.equalsIgnoreCase(announcementTime) && shouldBeAnnounced) {
                System.out.println("Sending message with ID " + voiceStore.getId());
                //Execute 192.168.2.33:1880/googlehome?announcement=hallo%20world
                String messageUrl = url + messagePrefix + " " + voiceStore.getVoiceMessage();

                voiceStore.setAnnounced("Y");
                voiceStoreService.updateAnnouncement(voiceStore);

                try {
                    restTemplate.exchange(messageUrl, HttpMethod.GET, null, Void.class);
                    System.out.println("done");
                } catch (Exception ex) {
                    System.out.println("ex: " + ex.getMessage());
                }
            }
        }
    }
}

