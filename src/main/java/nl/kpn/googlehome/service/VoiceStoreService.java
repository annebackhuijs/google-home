package nl.kpn.googlehome.service;

import nl.kpn.googlehome.model.VoiceStore;
import nl.kpn.googlehome.repository.VoiceStoreRepository;
import nl.kpn.googlehome.vo.Announcement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class VoiceStoreService {
    @Autowired
    VoiceStoreRepository voiceStoreRepository;

    public void saveVoiceMessage(List<Announcement> announcementList) {

        announcementList.forEach(announcement -> {
            VoiceStore voiceStore = new VoiceStore();
            voiceStore.setCreateDate(new Date());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date formatedDate = null;
            try {
                formatedDate = simpleDateFormat.parse(announcement.getDate()+" "+announcement.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            voiceStore.setAnnouncementDate(formatedDate);
            voiceStore.setVoiceMessage(announcement.getAnnouncement());
            voiceStore.setShow(announcement.getShow());
            voiceStore.setVoiceReporter(announcement.getReporter());
            voiceStoreRepository.save(voiceStore);
        });
    }

    public List<Announcement> getAnnouncements()
    {
        List<Announcement> announcements = new ArrayList<>();
        List<VoiceStore> voiceStoreList = voiceStoreRepository.findAll();
        voiceStoreList.forEach(voiceStore -> {
            Announcement announcement = new Announcement();
            DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat sdfTime  = new SimpleDateFormat("HH:mm");
            //String dateOnly = simpleDateFormat.format(voiceStore.getAnnouncementDate());
            announcement.setDate(simpleDateFormat.format(voiceStore.getAnnouncementDate()));
            announcement.setTime(sdfTime.format(voiceStore.getAnnouncementDate()));
            announcement.setAnnouncement(voiceStore.getVoiceMessage());
            announcement.setReporter(voiceStore.getVoiceReporter());
            announcement.setShow(voiceStore.getShow());
            announcements.add(announcement);
        });

        return announcements;
    }

    public List<VoiceStore> getVoicestoreList()
    {
        List<VoiceStore> voiceStoreList = voiceStoreRepository.findAll();

        return voiceStoreList;
    }

    public void updateAnnouncement(VoiceStore voiceStore){
        voiceStoreRepository.save(voiceStore);
    }
}
