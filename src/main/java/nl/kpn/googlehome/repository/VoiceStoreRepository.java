package nl.kpn.googlehome.repository;

import nl.kpn.googlehome.model.VoiceStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoiceStoreRepository extends JpaRepository<VoiceStore,Long> {
}
