package nl.kpn.googlehome.model;

import lombok.Data;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "voice_store")
public class VoiceStore {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    @Column(
            name = "voice_message",
            nullable = false
    )
    private String voiceMessage;

    @Column(
            name = "voice_reporter",
            nullable = false
    )
    private String voiceReporter;

    @Column(
            name = "create_date",
            nullable = false
    )
    private Date createDate;

    @Column(
            name = "announcement_date",
            nullable = false
    )
    private Date announcementDate;

    @Column(
            name = "show_message"
    )
    private String show;

    @Column(
            name = "announced"
    )
    private String announced;

}
