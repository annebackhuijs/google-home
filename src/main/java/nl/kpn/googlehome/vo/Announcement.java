package nl.kpn.googlehome.vo;

import lombok.Data;


@Data
public class Announcement {
    private String date;
    private String time;
    private String announcement;
    private String reporter;
    private String show;
}
