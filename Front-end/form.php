<?php

$myFile = "data.json";
$arr_data = array(); // create empty array

	   //Get form data
	   $formdata = array(
        'date'=> $_POST['date'],
        'time'=> $_POST['time'],
        'announcement'=>$_POST['announcement'],
        'reporter'=> $_POST['reporter'],
        'show'=> $_POST['show']
     );

	   //Get data from existing json file
       $jsondata = file_get_contents($myFile);
       
        // converts json data into array
	   $arr_data = json_decode($jsondata, true);

	   // Push user data to array
       array_push($arr_data,$formdata);
       
       //Convert updated array to JSON
	   $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);
	   
	   //write json data into data.json file
	   if(file_put_contents($myFile, $jsondata)) {
	        echo 'Data successfully saved';
	    }
	   else {
            echo "error";
       }


    //    TODO: 
    //    Go back to index.html
    header("Location: index.html");
            
?>