var _month = 0;
var _currentWeekNo = moment().week();
var _view = 1;

$(document).ready(function() {
    handleView(_view); // set default view
    createCalendar(moment().startOf('month').week(), moment().endOf('month').week());
    displaySelectedMonth();
});

function handleView($this) {
    _view = $this;
    var views = ['form', 'list', 'calendar'];
    $('[class*=-view]').hide();
    $('.' + views[$this] + '-view').show();
}

function createCalendar(startWeek, endWeek) {
    let year = moment().year();
    if (_currentWeekNo > endWeek) {
        year++; // TODO: check for next 2 years
    }

    if (startWeek > endWeek && startWeek < 52) {
        endWeek = 52 + endWeek;
    }

    let calendar = []
    for (var week = startWeek; week < endWeek + 1; week++) {
        calendar.push({
            week: week,
            days: Array(7).fill(0).map((n, i) => moment().week(week).weekYear(year).startOf('week').clone().add(n + i, 'day'))
        })

    }
    printCalendar(calendar);
}

function printCalendar(cal) {
    var destination = $('table#calendar>tbody');
    destination.empty();
    $.each(cal, function(key, val) {
        destination.append('<tr id="week-' + val.week + '">');

        $.each(val.days, function(key, day) {
            var dayNo = moment(day).format('D');
            var monthNo = moment(day).format('M');
            var activeState = currentMonth(monthNo);
            var today = moment().isSame(moment(day), 'day');

            $('#week-' + val.week).append('<td id="' + dayNo + '-' + monthNo + '" class="' + activeState + ' ' + today + '"><span>' + dayNo + '</span></td>');
        });
    });
    getData();
}

function getData() {

    $.getJSON('./data.json', function(data) {
        printTable(data);
        addEventsToCalendar(data);
    });


}

function addEventsToCalendar(events) {
    $.each(events, function(key, val) {
        var today = moment().add(_month, 'months');
        if (moment(val.date).isSame(today, 'month')) {
            var day = '#' + moment(val.date).format('D') + '-' + moment(val.date).format('M');
            $(day).append('<p class="' + val.show + '">' + val.announcement + '</p>');
        }
    })
}

function printTable(data) {
    var table = $('#announcement-table');
    table.empty();

    $.each(data, function(key, val) {
        var HTML = '<tr><td><span class="big">' + moment(val.date).format('D') + '</span><p>' + moment(val.date).format('MMM YYYY') + '</p></td><td><p class="' + val.show + '">' + val.time + '</p></td><td><p>' + val.announcement + '</p></td><td><p>' + val.reporter + '</p></td><td><p>' + val.show + '</p></td></tr>';
        table.append(HTML);
    })
}

function currentMonth(no) {
    const thisMonth = moment().add(_month, 'months').format('M');
    if (thisMonth !== no) {
        return 'inactive';
    } else {
        return '';
    }

}

function displaySelectedMonth() {
    const thisMonth = moment().format('MMMM YYYY');
    $('.calendar-view a:nth-child(3)').after('<h3>' + thisMonth + '</h3>');
}

function nextMonth(val) {
    _month = _month + val;
    const thisMonth = moment().add(_month, 'months').format('MMMM YYYY');
    $('.calendar-view h3').text(thisMonth);
    var startWeek = moment().add(_month, 'months').startOf('month').week();
    var endweek = moment().add(_month, 'month').endOf('month').week();

    createCalendar(startWeek, endweek);

}

function sortTable($this) {
    var asc = $($this).hasClass('asc');
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("list");
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            //check if the two rows should switch place:
            if (asc) {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }

        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            if (asc) {
                $($this).removeClass('asc');
                $($this).addClass('desc');
            } else {
                $($this).removeClass('desc');
                $($this).addClass('asc');
            }
        }
    }
}