package nl.kpn.googlehome.controller;

import nl.kpn.googlehome.service.VoiceStoreService;
import nl.kpn.googlehome.vo.Announcement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class CalendarController {

    @Autowired
    VoiceStoreService voiceStoreService;

    @GetMapping(value = "/api/announcements", produces = {MediaType.APPLICATION_JSON_VALUE})
    private List<Announcement> retrieveCalendar() {
        return voiceStoreService.getAnnouncements();
    }

    @PostMapping(value = "/api/calendar")
    private String addAnnouncement(@RequestBody List<Announcement> announcements) {
        voiceStoreService.saveVoiceMessage(announcements);
        return "Announcements created";
    }

}
